# Rust Life

## Invocation
```
  cargo run --release
  cargo run --release -- --board=N --population=M
  cargo run --release -- --from=file_with_initial_seed.txt
```
  
## Details

Quick and dirty implementation of
[Conway's Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
in
[rust](https://www.rust-lang.org/).

Using
[Rayon](https://docs.rs/rayon/latest/rayon/)
to reap some parallelization benefits.

Storage is sparse, using a `HashSet` to represent populated points.

Visualization is still really rough, just dumping `.` and `#` to the terminal.
