use std::fs::File;
use std::io::{BufRead, BufReader};

use clap::Parser;
use hashbrown::HashSet;
use rand::distributions::{Distribution, Uniform};
use rayon::prelude::*;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// file to read initial state from
    #[arg(short, long)]
    from: Option<String>,
    /// starting board size
    #[arg(short, long, default_value_t = 30)]
    board: i64,
    /// starting populate (how many cells)
    #[arg(short, long, default_value_t = 200)]
    population: u32,
}

#[derive(Clone,Copy,Debug,Hash,PartialEq,Eq)]
struct Point {
    x: i64,
    y: i64,
}
impl Point {
    fn neighbors(&self) -> Vec<Point> {
	let mut nbr: Vec<Point> = vec![];
	for x in (self.x - 1)..=(self.x + 1) {
	    for y in (self.y - 1)..=(self.y + 1) {
		if x == self.x && y == self.y {
		    continue;
		}
		nbr.push(Point { x, y });
	    }
	}
	nbr
    }
    fn count_neighbors(&self, board: &HashSet<Point>) -> usize {
	self.neighbors().par_iter()
	    .filter(|x| board.contains(*x))
	    .count()
    }

}

#[test]
fn test_neighbors() {
    let pnt = Point { x: 0, y: 0 };
    assert_eq!(
        pnt.neighbors(),
        vec![
    	Point { x: -1, y: -1 },
    	Point { x: -1, y:  0 },
    	Point { x: -1, y:  1 },
    	Point { x:  0, y: -1 },
    	Point { x:  0, y:  1 },
    	Point { x:  1, y: -1 },
    	Point { x:  1, y:  0 },
    	Point { x:  1, y:  1 },
        ]
    );
}
#[test]
fn test_count_neighbors() {
    let pnt = Point { x: 0, y: 0 };
    let board: HashSet<Point> = HashSet::from([
        pnt,
        Point { x: -1, y: -1 },
        Point { x: -1, y:  0 },
        Point { x: -1, y:  1 },
        Point { x:  1, y:  0 },
        Point { x:  1, y:  1 },
        Point { x:  1, y:  2 },
    ]);
    assert_eq!(pnt.count_neighbors(&board), 5);
}

fn main() {
    let args = Args::parse();
    let mut board: HashSet<Point> = match args.from {
	Some(file) => load_board(file),
	None => random_board(args),
    };

    let mut gens: Vec<HashSet<Point>> = vec![];
    loop {
	gens.push(board.clone());
	visualize(&board, gens.len(), true);
	board = age(&board);
	if gens.contains(&board) {
	    println!("hit a stable loop after {} generations", gens.len());
	    visualize(&gens[0], 1, false);
	    break;
	}
    }
}

fn load_board(filename: String) -> HashSet<Point> {
    let file = File::open(filename).unwrap();
    let input =  BufReader::new(file);
    let mut board: HashSet<Point> = HashSet::new();
    for (y, l) in input.lines().enumerate() {
	let line = l.unwrap();
	for (x, c) in line.chars().enumerate() {
	    if c == '#' {
		board.insert(Point { x: x as i64, y: y as i64 });
	    }
	}
    }
    board
}

#[test]
fn test_load_board() {
    let mut expect: HashSet<Point> = HashSet::new();
    for y in 0..100 {
	expect.insert(Point {x: 0, y });
	expect.insert(Point {x: y % 10, y });
	expect.insert(Point {x: 9, y });
	expect.insert(Point {x: 19, y });
	expect.insert(Point {x: 29, y });
	expect.insert(Point {x: 39, y });
	expect.insert(Point {x: 49, y });
	expect.insert(Point {x: 59, y });
	expect.insert(Point {x: 69, y });
	expect.insert(Point {x: 79, y });
    }
    assert_eq!(expect, load_board("src/test.txt".to_string()));
}

fn random_board(args: Args) -> HashSet<Point> {
    let mut board: HashSet<Point> = HashSet::new();
    let mut rng = rand::thread_rng();
    let die = Uniform::from(0..args.board);
    // initialize the board
    for _idx in 0..args.population {
	board.insert(Point { x: die.sample(&mut rng), y: die.sample(&mut rng) });
    }
    board
}

fn bounds (board: &HashSet<Point>) -> (Point, Point) {
    let mut points: Vec<&Point> = board.par_iter().collect();
    points.par_sort_by(|a, b| a.x.partial_cmp(&b.x).unwrap());
    let min_x = points.first().unwrap().x;
    let max_x = points.last().unwrap().x;
    points.par_sort_by(|a, b| a.y.partial_cmp(&b.y).unwrap());
    let min_y = points.first().unwrap().y;
    let max_y = points.last().unwrap().y;
    (Point { x: min_x, y: min_y }, Point {x: max_x, y: max_y })
}

fn age(board: &HashSet<Point>) -> HashSet<Point> {
    let mut new: HashSet<Point> = HashSet::new();
    let (min, max) = bounds(board);
    // add one to test the border around populated ponts
    for x in min.x-1..=max.x+1 {
	for y in min.y-1..=max.y+1 {
	    let point = Point { x, y };
	    let count = point.count_neighbors(board);
	    if board.contains(&point) {
		if count == 2 || count == 3 {
		    // let live
		    new.insert(point);
		}
	    }
	    else  if count == 3 {
		// reproduction
		new.insert(point);
	    }
	}
    }
    new
}

fn visualize(board: &HashSet<Point>, gen: usize, blank: bool) {
    let (min, max) = bounds(board);
    println!("generation: {gen}");
    for y in min.y..=max.y {
	for x in min.x..=max.x {
	    if board.contains(&Point { x, y }) {
		print!("#");
	    }
	    else {
		print!(".");
	    }
	}
	println!();
    }
    if blank {
	// clear the screen
	print!("\x1B[2J");
    }
    else {
	println!();
    }
}
